# Get started with a basic, but nicely working system as you are learning Reactjs

### There are very few steps to get you up and running

- You will need to have nodejs and npm already installed. Follow instructions at https://nodejs.org/
- Clone the project:
  `git clone https://rohanthewiz@bitbucket.org/rohanthewiz/react_barebones.git`
- Change to the repo directory  `cd react_barebones`
- Install project dependencies:  `npm install`
- Load up the example in a webserver with  `npm start`
- View the output in your browser by navigating to `localhost:3333` (CTRL-C to exit the server)
- You are set! Find a good tutorial and start writing react apps.

Thanks to [Geoforce](http://geoforce.com/ "World class asset tracking") for providing me with awesome opportunities!